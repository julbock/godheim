﻿using UnityEngine;
using System.Collections;

public class LokiHumanAnimationScript : MonoBehaviour {
	public GameObject Stop, Move, Jump, Fall, Land;
	public float JumpDelayTime, LandDelayTime;
	private string previousPosition;
	private string currentPosition;
	private float jumpTime, landTime;
	
	// Use this for initialization
	void Start ()
	{
		previousPosition = gameObject.GetComponent<CharacterMovementScript>().MovementState;
		Stop = transform.Find ("Stop").gameObject;
		Move = transform.Find ("Move").gameObject;
		Jump = transform.Find ("Jump").gameObject;
		Fall = transform.Find ("Fall").gameObject;
		Land = transform.Find ("Land").gameObject;
		jumpTime = 0;
		landTime = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		currentPosition = gameObject.GetComponent<CharacterMovementScript>().MovementState;
		// print(currentPosition);
		if (previousPosition == "Jump")
		{
			jumpTime += Time.deltaTime;
			if (jumpTime >= JumpDelayTime)
			{
				jumpTime = 0;
				SetAnimation("Fall");
				previousPosition = currentPosition = "Fall";
			}
		}
		else if (previousPosition == "Fall")
		{
			if (currentPosition == "Jump") 
			{
				previousPosition = "Jump";
				landTime = 0;
				SetAnimation("Jump");
			}
			else if (currentPosition == "Stop" || currentPosition == "Move")
			{
				landTime += Time.deltaTime;
				if (landTime >= LandDelayTime)
				{
					SetAnimation(currentPosition);
					previousPosition = currentPosition;
					landTime = 0;
				}
			}
		}
		else
		{
			if (currentPosition == "Stop" && previousPosition != "Stop")
			{
				SetAnimation(currentPosition);
				previousPosition = currentPosition;
			}
			else if (currentPosition == "Move" && previousPosition != "Move")
			{
				SetAnimation(currentPosition);
				previousPosition = currentPosition;
			}
			else if (currentPosition == "Jump")
			{
				SetAnimation(currentPosition);
				previousPosition = currentPosition;
			}
			else if (currentPosition == "Fall")
			{
				SetAnimation(currentPosition);
				previousPosition = currentPosition;
			}
		}
	}
	
	void SetAnimation(string animation)
	{
		Stop.SetActive(false);
		Move.SetActive(false);
		Jump.SetActive(false);
		Fall.SetActive(false);
		Land.SetActive(false);
		transform.Find(animation).gameObject.SetActive(true);
	}
}