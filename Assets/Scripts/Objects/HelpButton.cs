﻿using UnityEngine;
using System.Collections;

public class HelpButton : MonoBehaviour 
{
	private CharacterScript charScript = null;

	public bool lokiCollide = true;
	public bool thorCollide = true;
	private bool inContact = false;

	void Start () 
	{
		renderer.enabled = false;
	}

	void Update () 
	{
		if (inContact)
		{
			renderer.enabled = true;
		}
		else
		{
			renderer.enabled = false;
		}
	}

	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		CharacterScript charScript2 = otherCollider.gameObject.GetComponent<CharacterScript>();
		if (charScript2)
			if ((charScript2.character == "Thor" && !thorCollide) || (charScript2.character == "Loki" && !lokiCollide))
				return;
		charScript = charScript2;
		
		if (charScript)
		{
			if ((charScript.character == "Thor" && thorCollide) || (charScript.character == "Loki" && lokiCollide))
			{
				inContact = true;
			}
		}
	}
	
	void OnTriggerExit2D(Collider2D otherCollider)
	{
		CharacterScript charScript = otherCollider.gameObject.GetComponent<CharacterScript>();
		
		if (charScript)
			if ((charScript.character == "Thor" && !thorCollide) || (charScript.character == "Loki" && !lokiCollide))
				return;
		
		if (charScript != null)
		{
			if ((charScript.character == "Thor") || (charScript.character == "Loki"))
			{
				inContact = false;
			}
		}
	}

}
