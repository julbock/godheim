﻿using UnityEngine;
using System.Collections;

/// <summary>
/// General implementation of the items that can be caught, and who they are able to collide with.
/// </summary>
public class ItemScript : MonoBehaviour 
{
	public string Name = "none";
	public bool lokiCollide = true;
	public bool thorCollide = true;

	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		// Did it collide with a character?
		CharacterScript charScript = otherCollider.gameObject.GetComponent<CharacterScript>();
		if (charScript != null)
		{
			//If the item collided with a character, and it is supposed to collide with that specific character
			if ((charScript.character == "Loki" && lokiCollide) || (charScript.character == "Thor" && thorCollide))
			{
				otherCollider.gameObject.GetComponentInParent<ItemManager>().setItem = name;
				gameObject.SetActive(false);
			}
		}
	}
}
