﻿using UnityEngine;
using System.Collections;

public class RotatingTrapScript : MonoBehaviour 
{
	private float time;

	void Start ()
	{
		time = Mathf.PI/2;
	}

	void Update () 
	{
		time += 2*Time.deltaTime;
		if (time >= 2 * Mathf.PI) time -= 2 * Mathf.PI;
	}

	void FixedUpdate () 
	{
//		print (transform.parent.gameObject.rigidbody2D.rotation);
		transform.parent.gameObject.rigidbody2D.angularVelocity = 100 * Mathf.Sin(time);
	}
}
