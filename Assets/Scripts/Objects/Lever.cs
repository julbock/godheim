﻿using UnityEngine;
using System.Collections;

public class Lever : MonoBehaviour 
{
	private bool inContact = false;
	private bool rotate = false;
	public bool activate = false;
	public bool lokiCollide = true;
	public bool thorCollide = false;
	private CharacterScript charScript = null;
	void Start () {}

	private string actionButton = "r";
	private string actionButton2 = "r";
	private string actionButtonUsed = "r";

	// Update is called once per frame
	void Update () 
	{
		if (charScript != null)
		{
			actionButton = charScript.character == "Loki" ? "e" : "u";
			actionButton2 = charScript.character == "Loki" ? "joystick button 2" : "joystick button 3";

			if (Input.GetKeyDown(actionButton) || Input.GetKeyDown(actionButton2))
			{
				rotate = inContact;
				charScript.interacting = rotate;
				actionButtonUsed = Input.GetKeyDown(actionButton) ? actionButton : actionButton2;
			}
			if(Input.GetKeyUp(actionButtonUsed))
			{
				rotate = false;
			}
		}
	}
	
	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		CharacterScript charScript2 = otherCollider.gameObject.GetComponent<CharacterScript>();
		if (charScript2)
			if ((charScript2.character == "Thor" && !thorCollide) || (charScript2.character == "Loki" && !lokiCollide))
				return;
		charScript = charScript2;

		if (charScript)
		{
			if ((charScript.character == "Thor" && thorCollide) || (charScript.character == "Loki" && lokiCollide))
			{
				inContact = true;
			}
		}
	}
	
	void OnTriggerExit2D(Collider2D otherCollider)
	{
		CharacterScript charScript = otherCollider.gameObject.GetComponent<CharacterScript>();

		if (charScript)
			if ((charScript.character == "Thor" && !thorCollide) || (charScript.character == "Loki" && !lokiCollide))
				return;

		if (charScript != null)
		{
			if ((charScript.character == "Thor") || (charScript.character == "Loki"))
			{
				charScript.interacting = false;
				inContact = false;
				rotate = false;
			}
		}
	}
	
	void FixedUpdate()
	{
        if(rotate && rigidbody2D.rotation<90)
        {
            rigidbody2D.angularVelocity = 100;
        }
        else
        {
            if(rotate && rigidbody2D.rotation>=90)
            {
                activate = true;
                rigidbody2D.angularVelocity = 0;
            }
            if(!rotate)
            {
                activate = false;
                if(rigidbody2D.rotation<=0)
                {
					if(charScript != null)
					{
						charScript.interacting = false;
					}
                	rigidbody2D.angularVelocity = 0;
                }
                else
                {
                    rigidbody2D.angularVelocity = -100;
                }
            }
        }
	}
}