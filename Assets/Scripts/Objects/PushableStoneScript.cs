﻿using UnityEngine;
using System.Collections;

public class PushableStoneScript : MonoBehaviour 
{
	private CharacterScript charScript = null;
	private bool inContact;
	void Start () {}

	void Update() {}

	void FixedUpdate () 
	{
		if (inContact)
		{
			if (Input.GetKeyDown("u") || Input.GetKeyDown("joystick button 3"))
				rigidbody2D.velocity = new Vector2(1.5f,0);
		}
	}

	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		CharacterScript charScript2 = otherCollider.gameObject.GetComponent<CharacterScript>();
		if (charScript2)
			if (charScript2.character == "Loki")
				return;
		
		charScript = charScript2;
		
		if (charScript)
		{
			if ((charScript.character == "Thor"))
			{
				inContact = true;
			}
		}
	}
	
	void OnTriggerExit2D(Collider2D otherCollider)
	{
		CharacterScript charScript = otherCollider.gameObject.GetComponent<CharacterScript>();
		
		if (charScript != null)
		{
			if (charScript.character == "Thor")
			{
				inContact = false;
				rigidbody2D.velocity = new Vector2(0,0);
			}
		}
	}
}
