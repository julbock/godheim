﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Stores the information of all the items already caught.
/// </summary>
public class ItemManager : MonoBehaviour 
{
	public bool eagleItem = false;

	void Start () {}
	
	void Update () {}

	public string setItem
	{
		set
		{
			string itemName = value;
			if (itemName == "eagleItem")
				eagleItem = true;
		}
	}
}
