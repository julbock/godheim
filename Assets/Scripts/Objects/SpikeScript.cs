﻿using UnityEngine;
using System.Collections;

public class SpikeScript : MonoBehaviour 
{
	public int damage = 1;
	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {}

	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		// Did it collide with a character?
		CharacterScript charScript = otherCollider.gameObject.GetComponent<CharacterScript>();
		if (charScript != null)
		{
			//If the item collided with a character, and it is supposed to collide with that specific character
			if ((charScript.character == "Loki") || (charScript.character == "Thor") || (charScript.character == "Eagle"))
			{
				otherCollider.gameObject.GetComponent<HealthScript>().Damage(damage);
			}
		}
	}
}
