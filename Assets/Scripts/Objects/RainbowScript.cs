﻿using UnityEngine;
using System.Collections;

public class RainbowScript : MonoBehaviour 
{
	public GameObject lever;
	public GameObject gate;
	private bool activated = false;
	GameObject rainbowCollider;

	void Start ()
	{
		renderer.enabled = false;
		rainbowCollider = GameObject.Find("rainbowFloor");
		rainbowCollider.gameObject.SetActive(false);
	}
	
	void Update () 
	{
		renderer.enabled = lever.GetComponent<Lever>().activate || activated;
		if (lever.GetComponent<Lever>().activate)
		{
			activated = true;
			rainbowCollider.gameObject.SetActive(true);
		}
		if (activated)
			gate.gameObject.GetComponentInChildren<Gate>().rainbowIsUp = true; //should activate the lever of the second gate after the rainbow is up
	}
}
