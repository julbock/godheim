﻿using UnityEngine;
using System.Collections;

public class Bridge : MonoBehaviour {

	// Use this for initialization

	private bool rotate = false;
    public GameObject go;
    private Lever lever;
	void Start () {
        
	
	}
	
	// Update is called once per frame
	void Update () 
	{
        lever = go.GetComponent<Lever>();
        rotate = lever.activate;
	}

	void FixedUpdate()
	{
        if(rotate && rigidbody2D.rotation>0)
		{
			rigidbody2D.angularVelocity = -50;
        }
        else
        {
            if(!rotate && rigidbody2D.rotation< 100)
            {
                rigidbody2D.angularVelocity = 50;
            }
            else
            {
                rigidbody2D.angularVelocity = 0;
            }
        }
	}
}
