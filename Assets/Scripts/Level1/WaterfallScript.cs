﻿using UnityEngine;
using System.Collections;

public class WaterfallScript : MonoBehaviour 
{
	private bool hasPlayed = false;

	void Start () 
	{
		audio.Stop();
	}

	void Update() {}

	void OnTriggerEnter2D(Collider2D otherCollider) 
	{
		if (otherCollider.GetComponent<CharacterScript>())
		{
			if (hasPlayed == false)
			{
				audio.Play();
				audio.loop = true;
				hasPlayed = true;
			}
		}
	}
}
