﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public GameObject Thor, Loki;
	private bool someoneDied = false;
	[HideInInspector]
	public bool finishedLevel;
	private float time = 0, timeToMenu = 18f;
	private bool lokiExit = false, thorExit = false;
	private GameObject EndTheme;

	// Use this for initialization
	void Start ()
	{
		EndTheme = transform.FindChild("EndTheme").gameObject;
		EndTheme.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (lokiExit && thorExit) finishedLevel = true;
		if (!Thor) someoneDied = true;
		if (!Loki.transform.FindChild("Eagle"))
			someoneDied = true;
		if (!Loki.transform.FindChild("Human"))
			someoneDied = true;
		if (Input.GetKey("escape"))
			Application.LoadLevel("Menu");
		if (someoneDied)
		{
			time += Time.deltaTime;
			if (time >= timeToMenu)
				Application.LoadLevel("Menu");
		}
		if (finishedLevel)
		{
			if (GameObject.Find ("Players"))
			{
				GameObject.Find ("Players").SetActive(false);
				transform.FindChild("MainTheme").gameObject.SetActive(false);
				EndTheme.SetActive(true);
			}
			time += Time.deltaTime;
			if (time >= timeToMenu)
				Application.LoadLevel("EndGame");
		}
	}

	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		if (otherCollider.GetComponent<EagleScript>())
			lokiExit = true;
		if (otherCollider.GetComponent<LokiScript>())
			lokiExit = true;
		if (otherCollider.GetComponent<ThorAnimationScript>())
			thorExit = true;
	}

	void OnTriggerExit2D(Collider2D otherCollider)
	{
		if (otherCollider.GetComponent<EagleScript>())
			lokiExit = false;
		if (otherCollider.GetComponent<LokiScript>())
			lokiExit = false;
		if (otherCollider.GetComponent<ThorAnimationScript>())
			thorExit = false;
	}
}
