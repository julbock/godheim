﻿using UnityEngine;
using System.Collections;

public class CloudCycle : MonoBehaviour 
{
	public float scrollSpeed = 10;
	public float tileSizeZ;
	public GameObject environment;
	public int order = 1;
	
	private Vector2 startPosition;
	private SpriteRenderer rend;
	private SpriteRenderer envRend;
	private float width;
	private Vector2 basePosition;
	
	void Start ()
	{
		startPosition = transform.position;
		rend = GetComponent<SpriteRenderer>();
		envRend = environment.GetComponent<SpriteRenderer>();
		width = rend.bounds.max.x - rend.bounds.min.x;
		basePosition = new Vector2(envRend.bounds.max.x + width/2,transform.position.y);

	}
	
	void Update ()
	{
		//print(Time.time * scrollSpeed + startPosition.x);
		//print(2*width);
		float newPosition = Mathf.Repeat(Time.time * scrollSpeed + startPosition.x, 2*width);
		//print(newPosition);
		transform.position = basePosition - Vector2.right * newPosition;
	}
}
