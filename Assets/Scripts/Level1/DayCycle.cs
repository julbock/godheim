﻿using UnityEngine;
using System.Collections;

public class DayCycle : MonoBehaviour
{
	public float scrollSpeed = 1;
	public float tileSizeZ;
	
	private Vector2 startPosition;
	private SpriteRenderer rend;
	private float height;
	
	void Start ()
	{
		startPosition = transform.position;
		rend = GetComponent<SpriteRenderer>();
		height = rend.bounds.max.y - rend.bounds.min.y;
	}
	
	void Update ()
	{
		float newPosition = Mathf.Repeat(Time.time * scrollSpeed, height);
		transform.position = startPosition + Vector2.up * newPosition;
	}
}

/*public class DayCycle : MonoBehaviour 
{

	public int day_velocity = 100;

	private Vector2 movement;
	private bool translate = false;

	private SpriteRenderer rend;
	private float height;
	// Use this for initialization
	void Start () {
		rend = GetComponent<SpriteRenderer>();
		height = rend.bounds.max.y - rend.bounds.min.y;

	}
	
	// Update is called once per frame
	void Update () 
	{
		movement = new Vector2 (0, day_velocity);
		if(rend.bounds.min.y > 101.7f/2)
		{
			translate = true;
		}

	}

	void FixedUpdate ()
	{
		if(translate)
		{
			translate =  false;
			Vector2 reset = new Vector2(rigidbody2D.position.x, -101.7f);
			print (-height);
			rigidbody2D.position = reset;
			print(reset);
		}
		rigidbody2D.velocity = movement;
	}
}*/
