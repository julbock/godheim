﻿using UnityEngine;
using System.Collections;

public class LogAnimation1 : MonoBehaviour {

	public float deactivateTime;
	private float time;

	// Use this for initialization
	void Start () {
		time = 0;
		gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		if (time >= deactivateTime)
		{
			gameObject.SetActive(false);
		}
	}
}
