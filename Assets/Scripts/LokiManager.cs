﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Manages all of lokis transformations. It is responsible for activating and deactivating them.
/// </summary>
public class LokiManager : MonoBehaviour 
{
	public string form = "human";
	private string previousForm = "human";
	public float x, y;
	public GameObject eagle, human;
	private GameObject objectForm;

	// Finds a reference to all the forms loki can change to, and sets the initial form to human
	void Start () 
	{
		if (!eagle)
			eagle = GameObject.Find("Eagle");
		eagle.gameObject.SetActive(false);

		if (!human)
			human = GameObject.Find("Human");
	}
	
	// Updates lokis form whenever he changes it. The form must be written in lowercase letters
	void Update () 
	{
		if (previousForm != form)
		{
			previousForm = form;
			if (form == "eagle")
				objectForm = eagle;
			if (form == "human")
				objectForm = human;

			objectForm.SetActive(true);
			objectForm.transform.position = new Vector3(x,y,0);
			GameObject.Find("Barrier").GetComponent<BarrierScript>().Loki = objectForm.transform;
			GameObject.Find("Main Camera").GetComponent<CameraScript>().Loki = objectForm.transform;
		}
	}
}