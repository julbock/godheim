﻿using UnityEngine;
using System.Collections;

public class TextBox : MonoBehaviour
{
	public double blinkTime = 0.5;
	private double time;
	private bool blink;
	private GameObject text;

	// Use this for initialization
	void Start()
	{
		// Reference to the text child
		text = transform.Find("Text").gameObject;
		blink = false;
		time = 0;
	}

	void Update()
	{
		
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		time += (Time.fixedDeltaTime);
		if (time >= blinkTime)
		{
			time = 0;
			blink = !blink;
			text.SetActive(blink);
		}
	}
}
