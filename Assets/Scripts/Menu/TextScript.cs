﻿using UnityEngine;
using System.Collections;

public class TextScript : MonoBehaviour
{
	public string text = "Press start";
	public GUIStyle style;
	public float x, y, w, h;
	private GameObject parent;

	// Use this for initialization
	void Start ()
	{
		parent = transform.parent.gameObject;
		Vector3 screenPos = Camera.main.WorldToScreenPoint(parent.transform.position);
		// print("target is " + screenPos.x + " pixels from the left");
		// print("target is " + screenPos.y + " pixels from the bottom");
		x = screenPos.x;
		y =screenPos.y;
		// Vector3 screenPos2 = Camera.main.WorldToScreenPoint(transform.position);
		// print("I am " + screenPos2.x + " pixels from the left");
		// print("I am " + screenPos2.y + " pixels from the bottom");
	}

	// Update is called once per frame
	void Update()
	{
	}

	void FixedUpdate()
	{
	}

	void OnGUI ()
	{
		GUI.Label(new Rect(x, y, w, h), text, style);
	}
}
