﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour 
{
	public Transform Thor, Loki;
	public int levelID;
	private Vector3 pos;
	[HideInInspector]
	public float leftLimit, rightLimit, upperLimit, lowerLimit;
	[HideInInspector]
	public bool shouldIncrease = false;
	private float increaseTimer = 0f, increaseTime = 10f, increaseSpeed = 10f;
	private float size;

	void Start () 
	{
		DefaultLimits(levelID);
		pos = transform.position;
		pos.x = (Loki.position.x + Thor.position.x)/2;
		pos.y = (Loki.position.y + Thor.position.y)/2;
		transform.position = pos;
		size = GetComponent<Camera>().orthographicSize;
	}
	
	void Update () 
	{
		if (Thor && Loki)
		{
			print (shouldIncrease);
			if (shouldIncrease)
			{
				increaseTimer += Time.deltaTime;
				if (increaseTimer <= increaseTime)
				{
					size += increaseSpeed * Time.deltaTime;
					if (size > 7) size = 7;
					GetComponent<Camera>().orthographicSize = size;
				}
			}
			pos.x = (Loki.position.x + Thor.position.x)/2;
			if (pos.x < leftLimit) pos.x = leftLimit;
			if (pos.x > rightLimit) pos.x = rightLimit;
			pos.y = (Loki.position.y + Thor.position.y)/2;
			if (pos.y < lowerLimit) pos.y = lowerLimit;
			if (pos.y > upperLimit) pos.y = upperLimit;
			transform.position = pos;
		}
	}

	void DefaultLimits(int levelID)
	{
		switch (levelID)
		{
		case 1:
			leftLimit = (float)-49.6;
			rightLimit = (float)45;
			upperLimit = (float)45.7;
			lowerLimit = (float)-43.50248;
			break;
		default:
			break;
		}
	}
}
