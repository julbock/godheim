﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A general script all characters must use.
/// </summary>
public class CharacterScript : MonoBehaviour
{
	public string character = "Loki";
	public bool interacting = false;
	private CharacterMovementScript charMovement;
//	private SpriteRenderer sprite;
	private int standartLayer;

	private bool grounded = false;			// Whether or not the player is grounded.
	private Transform groundCheck;			// A position marking where to check if the player is grounded.
	private bool flip = false;

	void Start()
	{
		charMovement = gameObject.GetComponent<CharacterMovementScript>();
//		sprite = gameObject.GetComponent<SpriteRenderer>();
//		standartLayer = sprite.sortingOrder;
	}

	public bool isGrounded
	{
		get {return grounded;}
		set {grounded = value;}
	}

	public bool Flip
	{
		get {return flip;}
		set {flip = value;}
	}

	void Awake()
	{
		// Setting up references.
		groundCheck = transform.Find("groundCheck");
	}

	public void ChangeLayer(int new_order)
	{
		Vector3 new_pos;
		new_pos = transform.position;
		new_pos.z = new_order;
		transform.position = new_pos;
//		sprite.sortingOrder = new_order;
	}

	public void ResetLayer()
	{
		Vector3 new_pos;
		new_pos = transform.position;
		new_pos.z = 0;
		transform.position = new_pos;
//		sprite.sortingOrder = standartLayer;
	}
 
	void Update()
	{
		// Rotates the character when it moves to the opposite side
		if ((rigidbody2D.velocity.x < 0 && flip == false) || (rigidbody2D.velocity.x > 0 && flip == true))
		{
			gameObject.transform.Rotate(new Vector2(0,180));
			flip = !flip;
		}

		charMovement.move = !interacting;

		// The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
		grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
		grounded = grounded || Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Barrier"));
	}

	void FixedUpdate()
	{
		/*/Debug.Log(character);
		Debug.Log(interacting);
		Debug.Log(charMovement.move);*/
	}
}