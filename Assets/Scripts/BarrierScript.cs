﻿using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
[RequireComponent (typeof(Rigidbody2D), typeof(EdgeCollider2D))]
public class BarrierScript : MonoBehaviour
{
	public float Radius = 1.0f;
	public int NumPoints = 32;

	private Vector3 pos;
	EdgeCollider2D EdgeCollider;
	float CurrentRadius = 0.0f;

	public Transform Loki, Thor;

	void Start ()
	{
		CreateCircle();
		pos = transform.position;
		pos.x = (Loki.position.x + Thor.position.x)/2;
		pos.y = (Loki.position.y + Thor.position.y)/2;
		transform.position = pos;
	}
	
	void Update()
	{
		if (Loki && Thor)
		{
			if(NumPoints != EdgeCollider.pointCount || CurrentRadius != Radius)
			{
				CreateCircle();
			}
			pos.x = (Loki.position.x + Thor.position.x)/2;
			pos.y = (Loki.position.y + Thor.position.y)/2;
			transform.position = pos;
		}
	}

	void CreateCircle()
	{
		Vector2[] edgePoints = new Vector2[NumPoints + 1];
		EdgeCollider = GetComponent<EdgeCollider2D>();
		
		for(int loop = 0; loop <= NumPoints; loop++)
		{
			float angle = (Mathf.PI * 2.0f / NumPoints) * loop;
			edgePoints[loop] = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle)) * Radius;
		}
		
		EdgeCollider.points = edgePoints;
		CurrentRadius = Radius;
	}
}