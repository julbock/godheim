﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handle hitpoints and damages
/// </summary>
public class HealthScript : MonoBehaviour
{
	// Total hitpoints
	public int hp = 1;

	// Enemy or player?
	public bool isEnemy = false;

	// Inflicts damage and check if the object should be destroyed
	public void Damage(int damageCount)
	{
		hp -= damageCount;
		
		if (hp <= 0)
		{
			// Dead!
			Destroy(gameObject);
		}
	}
}