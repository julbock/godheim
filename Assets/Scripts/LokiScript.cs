﻿using UnityEngine;
using System.Collections;

/// <summary>
/// It is the script that defines loki when it is in human form.
/// </summary>
public class LokiScript : MonoBehaviour
{
	private string jumpButton = "w";
	private string jumpButton2 = "joystick button 4";
	private Transform eagle;

	void Update () 
	{
		if (gameObject.GetComponentInParent<ItemManager>().eagleItem == true)
		{
			if (!gameObject.GetComponent<CharacterScript>().isGrounded)
			{
				//If Loki tries to jump while in the air, it is supposed to become an eagle
				if (Input.GetKeyDown(jumpButton) || Input.GetKeyDown(jumpButton2))
				{
					gameObject.GetComponentInParent<LokiManager>().x = gameObject.transform.position.x;
					gameObject.GetComponentInParent<LokiManager>().y = gameObject.transform.position.y;
					gameObject.GetComponentInParent<LokiManager>().form = "eagle";
					gameObject.SetActive(false);
				}
			}
		}
	}
}