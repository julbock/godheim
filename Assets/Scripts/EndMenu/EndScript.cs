﻿using UnityEngine;
using System.Collections;

public class EndScript : MonoBehaviour {

	private float timer = 0, time = 2, menuTimer = 0;
	public float menuTime = 2;
	public float scaleSpeedX, scaleSpeedY, moveSpeed;
	private Vector3 pos, scale;
	private bool hasReachedPoint = false, hasReachedScaleX = false, hasReachedScaleY = false;

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		timer += Time.deltaTime;
		if (timer >= time)
		{
			menuTimer += Time.deltaTime;
			if (menuTimer >= menuTime)
				Application.LoadLevel("Menu");
			pos = transform.position;
			scale = transform.localScale;
			if (!hasReachedPoint)
			{
				pos.y += moveSpeed * Time.deltaTime;
				if (pos.y >= 4.7f && pos.y <= 4.9f)
				{
					pos.y = 4.8f;
					hasReachedPoint = true;
				}
			}
	
			if (!hasReachedScaleX)
			{
				scale.x += scaleSpeedX * Time.deltaTime;
				if (scale.x >= 0.09f && scale.x <= 1.01f)
				{
					scale.x = 1f;
					hasReachedScaleX = true;
				}
			}
	
			if (!hasReachedScaleY)
			{
				scale.y += scaleSpeedY * Time.deltaTime;
				if (scale.y >= 0.09f && scale.y <= 1.01f)
				{
					scale.y = 1f;
					hasReachedScaleY = true;
				}
			}
	
			transform.position = pos;
			transform.localScale = scale;
		}
	}
}
