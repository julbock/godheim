﻿using UnityEngine;
using System.Collections;

/// <summary>
/// It is the script that defines loki when he turns into an eagle.
/// </summary>
public class EagleScript : MonoBehaviour 
{
	private Transform groundCheck;			// A position marking where to check if the player is grounded.
	private bool flappedWings;

	void Start () {}

	void Awake()
	{
		groundCheck = transform.Find("groundCheck");
		flappedWings = false;
	}

	void Update () 
	{
		if (Input.GetKeyDown("s") || Input.GetKeyDown("joystick button 0"))
			changeBack();
		if (Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground")))
			changeBack();
		if (!flappedWings && (Input.GetKeyDown("joystick button 4") || Input.GetKeyDown("w")))
		{
			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, gameObject.GetComponent<CharacterMovementScript>().speed.y/4);
			flappedWings = true;
		}
	}

	void changeBack()
	{
		gameObject.GetComponentInParent<LokiManager>().x = gameObject.transform.position.x;
		gameObject.GetComponentInParent<LokiManager>().y = gameObject.transform.position.y;
		gameObject.GetComponentInParent<LokiManager>().form = "human";
		flappedWings = false;
		gameObject.SetActive(false);
	}

}
