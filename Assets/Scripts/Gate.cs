﻿using UnityEngine;
using System.Collections;

public class Gate : MonoBehaviour 
{
	private bool open = false;
	public GameObject go;
	private Lever lever;
	private float initial_position;
	public bool rainbowIsUp = true;
	void Start () 
	{
		initial_position = rigidbody2D.position.y;	
	}
	
	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		CharacterScript charScript;
		SpriteRenderer sprite = gameObject.GetComponent<SpriteRenderer>();
		if (charScript = otherCollider.gameObject.GetComponent<CharacterScript>())
			charScript.ChangeLayer(sprite.sortingOrder-1);
	}

	void OnTriggerExit2D(Collider2D otherCollider)
	{
		CharacterScript charScript;
		if (charScript = otherCollider.gameObject.GetComponent<CharacterScript>())
			charScript.ResetLayer();
	}
	
	// Update is called once per frame
	void Update () 
	{
		lever = go.GetComponent<Lever>();
		open = lever.activate || rainbowIsUp;
	}
	
	void FixedUpdate()
	{
		if(open && rigidbody2D.position.y - initial_position < 5)
		{
			rigidbody2D.velocity = new Vector2(0,10);
		}
		else
		{
			if(!open && rigidbody2D.position.y - initial_position > 0)
			{
				rigidbody2D.velocity = new Vector2(0,-10);
			}
			else
			{
				rigidbody2D.velocity = new Vector2(0,0);
			}
		}
	}
}
