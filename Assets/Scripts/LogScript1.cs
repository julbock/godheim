﻿using UnityEngine;
using System.Collections;

public class LogScript1 : MonoBehaviour {

	private bool isCharAbove;
	public float deactivationTime;
	public GameObject logAnimation;
	private float time;

	// Use this for initialization
	void Start ()
	{
		isCharAbove = false;
		time = 0;
	}

	// Update is called once per frame
	void Update ()
	{
		if (isCharAbove)
		{
			logAnimation.SetActive(true);
			time += Time.deltaTime;
			GetComponent<SpriteRenderer>().enabled = false;
			if (time >= deactivationTime)
				this.gameObject.SetActive(false);
		}
	}

	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		CharacterScript charScript = otherCollider.gameObject.GetComponent<CharacterScript>();
		if (charScript)
		{
			//If the item collided with a character, and it is supposed to collide with that specific character
			if ((charScript.character == "Loki") || (charScript.character == "Thor"))
			{
				isCharAbove = true;
			}
		}
	}
}
