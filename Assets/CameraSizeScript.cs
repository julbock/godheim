﻿using UnityEngine;
using System.Collections;

public class CameraSizeScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		if (otherCollider.GetComponent<ThorAnimationScript>())
		{
			Camera.current.GetComponent<CameraScript>().shouldIncrease = true;
		}
	}
}
